<?php
/**
 * @file
 * Custom template for the stealourcontent modal node.
 */
?>
<h1><?php print check_plain($title) ?></h1>
<?php
hide($content['comments']);
hide($content['links']);
?>

<div id="soc_modal_node_content">
  <?php print render($content); ?>
  <?php print render($content['submitted']); ?>
</div>
