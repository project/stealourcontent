<?php
/**
 * @file
 * Theme preprocessors for the StealOurContent module.
 */

/**
 * Theme preprocessor for the soc_button.tpl.php.
 */
function template_preprocess_soc_button(&$vars) {
  if ($vars['node']) {
    ctools_include('ajax');
    ctools_include('modal');
    ctools_modal_add_js();

    // Block config variables.
    $vars['soc_link_text'] = variable_get('soc_link_text', stealourcontent_text('soc_link_text'));
  }
}

/**
 * Theme preprocessor for soc_modal_window.tpl.php.
 */
function template_preprocess_soc_modal_window(&$vars) {
  $vars['soc_title'] = variable_get('soc_title', stealourcontent_text('soc_title'));

  $guidelines = variable_get('soc_guidelines', stealourcontent_text('soc_guidelines'));
  $vars['soc_guidelines'] = filter_xss_admin(token_replace($guidelines));

  $guidelines_link = variable_get('soc_guidelines_link', '');
  if (!empty($guidelines_link)) {
    $absolute = FALSE;
    if ((substr($guidelines_link, 0, 7) == 'http://') || (substr($guidelines_link, 0, 8) == 'https://')) {
      $absolute = TRUE;
    }
    $vars['soc_guidelines_link'] = l(t('View additional guideline details.'), $guidelines_link, array('absolute' => $absolute));
  }
  else {
    $vars['soc_guidelines_link'] = '';
  }

  $vars['soc_instructions'] = filter_xss_admin(variable_get('soc_instructions', stealourcontent_text('soc_instructions')));

  $branding = variable_get('soc_branding', stealourcontent_text('soc_branding'));
  $vars['soc_branding'] = filter_xss_admin(token_replace($branding, array('node' => $vars['node'])));

  $nodeview = node_view($vars['node'], 'stealourcontent_node', NULL);
  $rendered_node = render($nodeview);

  // If we can clean it up, try to.
  if (class_exists('tidy')) {
    $config = array(
      'wrap' => 0,
      'indent' => 1,
      'show-body-only' => 1,
      'drop-empty-paras' => 1,
    );

    $tidy = new tidy();
    $tidy->parseString($rendered_node, $config, 'utf8');
    $tidy->cleanRepair();
    $rendered_node = (string) $tidy;
  }

  // Convert relative urls to absolute.
  $vars['soc_rendered_node'] = soc_rel_to_abs($rendered_node);

  $vars['soc_tracking_pixel'] = NULL;
  if (module_exists('stealourcontent_stats')) {
    global $base_url;
    $vars['soc_tracking_pixel'] = theme('image', array(
        'path' => $base_url . '/stealourcontent/track.gif?nid=' . $vars['node']->nid,
        'attributes' => array('height' => '1', 'width' => '1'))
    );
  }

  // Use theme function so the license can be manually overridden.
  $vars['soc_license'] = theme('soc_license');

  $vars['display_submitted'] = variable_get('node_submitted_' . $vars['node']->type, TRUE);

  $account = user_load($vars['node']->uid);
  $author_url = url('user/' . $vars['node']->uid, array('absolute' => TRUE));
  $vars['author'] = theme('username', array(
      'account' => $account,
      'link_path' => $author_url,
      'link_options' => array('absolute' => TRUE))
      );

    // If there are any private files, display an appropriate message.
  $vars['soc_private_files'] = '';
  $files = soc_get_file_fields($vars['node']);
  $private = soc_private_files($files);
  if ($private) {
    $vars['soc_private_files'] = '<p><strong>' . t('Some files may not display correctly. These can be removed manually from the html.') . '</strong></p>';
  }
}

/**
 * Theme preprocessor for soc_license.tpl.php.
 *
 * Override this function or the template
 * to manually display a different license.
 */
function template_preprocess_soc_license(&$vars) {
  $licenses = stealourcontent_licenses();
  $license = variable_get('soc_license', STEALOURCONTENT_DEFAULT_LICENSE);
  $image = theme('image', array('path' => $licenses[$license]['image'], 'alt' => 'License'));
  $vars['soc_license_link'] = l($image, $licenses[$license]['link'], array('html' => TRUE, 'attributes' => array('target' => '_blank')));
}
